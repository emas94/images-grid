import ImagesGrid from "./components/ImagesGrid";
import styles from "./style.module.scss";

function App() {
  return (
    <div className={styles.app}>
      <ImagesGrid />
    </div>
  );
}

export default App;
