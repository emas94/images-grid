export interface ImageProps {
  name: string;
  url: string;
  id: number;
}
