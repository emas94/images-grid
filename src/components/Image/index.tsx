import classNames from "classnames";
import { ImageProps } from "../../interfaces";
import styles from "./style.module.scss";

interface Props {
  image: ImageProps;
  onClick: (id: number) => void;
  className?: string;
}
const Image = ({ image, onClick = () => {}, className }: Props) => (
  <div className={classNames(styles.wrapper, className)}>
    <img src={image.url} alt={image.name} onClick={() => onClick(image.id)} />
  </div>
);

export default Image;
