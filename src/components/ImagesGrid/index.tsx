import { useEffect, useMemo, useState } from "react";
import { useDebounce } from "../../hooks/useDebounce";
import { ImageProps } from "../../interfaces";
import Image from "../Image";
import styles from "./style.module.scss";

const ImagesGrid = () => {
  const imageSizeX = 300;
  const imageSizeY = 300;
  const imageClickLimit = 5;
  const defaultImageCount = 12;
  const [imageCount, setImageCount] = useState(defaultImageCount);
  const [images, setImages] = useState<ImageProps[] | null>(null);
  const debouncedInputValue = useDebounce(imageCount, 1000);
  const [counts, setCounts] = useState<number[]>(
    Array(defaultImageCount).fill(0)
  );

  const picsumUrlGenerator = (random: number) =>
    `https://picsum.photos/${imageSizeX}/${imageSizeY}?random=${random}`;
  const reloadImageClick = (id: number) => {
    if (images) {
      const index = images.findIndex((img) => img.id === id);
      const newImage = [...images];
      newImage[index].url = picsumUrlGenerator(Math.random());
    }
  };
  const handleImageClick = (id: number) => {
    if (images && counts) {
      const index = images.findIndex((img) => img.id === id);

      const newCounts = [...counts];
      if (index >= 0 && newCounts[index] < imageClickLimit) {
        newCounts[index] += 1;
        setCounts(newCounts);
        reloadImageClick(id);
      }
    }
  };
  const handleChangeImageCount = (value: number) => {
    setImageCount(value);
  };
  const handleResetCounts = () => {
    setCounts(Array(images?.length).fill(0));
  };
  const allImagesCount = useMemo(
    () => counts?.reduce((a, b) => a + b),
    [counts]
  );
  const generateMultipleImages = (count: number) => {
    return Array.from({ length: count }, (_, index) => ({
      name: `Image number: ${index + 1}`,
      url: picsumUrlGenerator(index + 1),
      id: index + 1,
    }));
  };

  useEffect(() => {
    if (images) {
      const sizeOfNewElements = images.length - counts.length;
      if (sizeOfNewElements >= 0 || sizeOfNewElements === 0) {
        setCounts(counts.concat(Array(sizeOfNewElements).fill(0)));
      } else {
        const newCounts = counts.filter(
          (_, index) => index < counts.length || index >= counts.length
        );

        setCounts(newCounts);
      }
    }
  }, [images]);
  useEffect(() => {
    if (debouncedInputValue >= 12 && debouncedInputValue <= 60) {
      setImages(() => generateMultipleImages(debouncedInputValue));
    } else {
      alert("enter a number from the range 12 ... 60");
    }
  }, [debouncedInputValue]);
  return (
    <div className={styles.wrapper}>
      <button onClick={handleResetCounts}>Reset</button>
      <div className={styles.imagesContainer}>
        {images?.map((i) => (
          <Image image={i} onClick={handleImageClick} key={i.id} />
        ))}
      </div>
      <input
        type="number"
        defaultValue={defaultImageCount}
        min="12"
        max="60"
        onChange={(e) => handleChangeImageCount(parseInt(e.target.value))}
      />
      <div className={styles.counterImageWrapper}>
        All image has been clicked: <span>{allImagesCount}</span> times.
      </div>
    </div>
  );
};

export default ImagesGrid;
